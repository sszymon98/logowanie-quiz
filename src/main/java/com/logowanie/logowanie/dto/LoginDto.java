package com.logowanie.logowanie.dto;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LoginDto {
    private String jsonString = null;
    private JSONObject jsonObject = null;
    private JSONArray arr = null;

    public LoginDto(){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/json/userdata.json"));
            jsonObject = (JSONObject) obj;
            jsonString = jsonObject.toJSONString();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getConfig(){
        return jsonString;
    }
    public JSONObject getDataJson(){
        return jsonObject;
    }
}
