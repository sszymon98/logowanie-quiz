package com.logowanie.logowanie.dto;

public class UserDto {
    private String login;
    private String password;

    public UserDto() {
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
