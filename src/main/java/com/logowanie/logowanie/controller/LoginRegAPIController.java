package com.logowanie.logowanie.controller;

import com.logowanie.logowanie.dto.LoginDto;
import com.logowanie.logowanie.dto.UserDto;
import org.json.simple.JSONArray;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;

@RestController
public class LoginRegAPIController {

    @RequestMapping(value = "/logowanie",method = RequestMethod.POST,produces={"application/json; charset=UTF-8"})
    public @ResponseBody ResponseEntity<String> logowanie(@RequestBody UserDto userDto){
        final LoginDto loginDto = new LoginDto();
        String loginJson = (String) loginDto.getDataJson().get("login");
        String passJson = (String) loginDto.getDataJson().get("password");
//        JSONArray arr = (JSONArray) loginDto.getDataJson().get("users");
//        Iterator<String> iterator = arr.iterator();
        //System.out.println(iterator.next().indexOf(0));

        if (loginJson.equals(userDto.getLogin()) && passJson.equals(userDto.getPassword())){
            return new ResponseEntity<String>("Zalogowano",HttpStatus.OK);
        }else{
            return new ResponseEntity<String>("Blędne dane logowania",HttpStatus.CONFLICT);
        }
    }
}
